from django.apps import AppConfig


class DishDomainConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dish_domain'
