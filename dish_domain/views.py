from django.db.models import Prefetch
from rest_framework import generics
from dish_domain.models import FoodListSerializer, FoodCategory, Food


class FoodList(generics.ListCreateAPIView):
    serializer_class = FoodListSerializer
    queryset = FoodCategory.objects.filter(food__isnull=False, food__is_publish=True).prefetch_related(
        Prefetch('food', queryset=Food.objects.filter(is_publish=True))).distinct()


class FoodListAll(generics.ListCreateAPIView):
    serializer_class = FoodListSerializer
    queryset = FoodCategory.objects.all()
